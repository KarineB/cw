<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @method static create(array $payload)
 */
class User extends Model
{
    use HasFactory;

    protected $fillable = ['name','address','password','passport_number'];


    /**
     * @return HasMany
     */
    public function bids()
    {
        return $this->hasMany(Bid::class);
    }
}
