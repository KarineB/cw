<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize():bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules():array
    {
        return [
            'name' => 'required|min:2',
            'passport_number' => ['required','unique:App\Models\User,passport_number'],
            'address' => 'required|max:255',
            'password' => 'required|confirmed|min:5'
        ];
    }
}
