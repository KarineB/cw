<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @param Request $request
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function index(Request $request)
    {
        if ($request->session()->has('user_id'))
        {
            $ticket = $request->session()->get('user_id');
            return view('welcome', compact('ticket'));

        }
        return redirect()->route('sessions.login')->with('error','You are not get ticket yet');
    }
}
