<?php

namespace App\Http\Controllers;

use App\Http\Requests\SessionRequest;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class SessionController extends AuthController
{
    /**
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     */
    public function create(Request $request)
    {
        if ($request->session()->has('user_id')) {
            return redirect()->route('home')->with('error', 'You are already login');
        }
        return view('sessions.create');
    }

    /**
     * @param SessionRequest $request
     * @return RedirectResponse
     */
    public function store(SessionRequest $request)
    {
        $user = User::where('passport_number', $request->get('passport_number'))->firstOrFail();
        if ($this->auth($user, $request->get('password'))) {
        $this->logIn($user);
        return redirect()->route('home')->with('success', 'You are get ticket successfully');
    }
       return back()->with('error','Incorrect passport number or password');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function destroy(Request $request)
    {
        if ($request->session()->has('user_id'))
        {
            $request->session()->remove('user_id');
            $request->session()->regenerate();
            return redirect()->route('sessions.login')->with('success','You are leave successfully!');
        }
        return redirect()->route('home')->with('error','You can not leave before get ticket =)!');
    }
}
