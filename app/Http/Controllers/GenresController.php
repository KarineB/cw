<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Book;
use App\Models\Genre;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class GenresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $genres = Genre::all();
        $books = Book::paginate(8);
       return view('genres.index',compact('genres','books'));
    }




    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Genre  $genre
     * @return Application|Factory|View
     */
    public function show(Genre $genre)
    {
        return view('genres.show',compact('genre'));
    }


}
