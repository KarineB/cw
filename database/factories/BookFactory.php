<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BookFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'status' => $this->faker->boolean(),
            'name' => $this->faker->title(),
            'picture' => 'pictures/' .$this->faker->image('public/storage/pictures',500,500,null,false),
            'genre_id' => rand(1,5),
            'author_id' => rand(1,10)
        ];
    }
}
