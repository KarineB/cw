<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BidFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'number' => $this->faker->unique()->numberBetween(1,100),
            'user_id' => rand(1,6),
            'book_id' => rand(1,20),
            'return' => $this->faker->dateTime()
        ];
    }
}
