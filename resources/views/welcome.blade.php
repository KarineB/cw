@extends('layouts.base')
@section('content')
    @include('notifications.alerts')
    <form action="{{route('sessions.delete')}}" method="post">
        @csrf
        @method('delete')
        <button type="submit">Logout</button>
    </form>
    <br>
    <div class="card-title">
        <h5>Your ticket number: {{md5($ticket)}}</h5>
    </div>
    <h5><a href="{{route('genres.index')}}">Библиотека</a></h5>
@endsection
