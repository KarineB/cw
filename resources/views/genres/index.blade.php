@extends('layouts.base')
@section('content')
    @include('notifications.alerts')
    <h1>Литература | <a href="">Мои книги</a>
        <h5><a href="{{route('sessions.login')}}">Авторизация</a></h5>
        <h5><a href="{{route('sessions.store')}}">Регистрация</a></h5>
        <hr class="line">
        <h4>Жанры:</h4>
        <div class="col">
        @foreach($genres as $genre)
        <a href="{{route('genres.show', ['genre' => $genre])}}" class="list-group-item list-group-item-action list-group-item-secondary">{{$genre->name}}</a>
        @endforeach
        </div>
        <hr class="line">
        @foreach($books as $book)
            <div class="container">
                <div class="card-group">
                    <img src="{{asset('/storage/' . $book->picture)}}" alt="pic-big-2" width="300" height="300">
                    <div class="card-body">
                        <h5 class="card-title">Book name: {{$book->name}}</h5>
                        <h5 class="card-title">Author: {{$book->author->name}}</h5>
                        <h5 class="card-title">Genre: {{$book->genre->name}}</h5>
                        @if($book->status)
                        <h5 class="card-title">Status: {{$book->status}}</h5>
                        <a href="{{route('bids.create')}}"><button type="submit">Получить</button></a>
                        @else Ожидается к 15.11.2021
                            @endif
                    </div>
                </div>
            </div>
            <br>
        @endforeach
        <div class="row">
            <div class="col-md-auto">
                {{$books->links('pagination::bootstrap-4')}}
            </div>
        </div>
@endsection
