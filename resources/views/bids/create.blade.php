@extends('layouts.base')
@section('content')
    <h1>Оформить заявку на получение книги</h1>
    @include('notifications.alerts')
    <form action="{{action([App\Http\Controllers\BidsController::class,'store'])}}" method="post">
        {{ csrf_field() }}

     <div class="card-title">Номер читательского билета {{md5($ticket)}}</div>

        <div class="form-group">
            <label for="return">Дата возвращения:</label><br/>
            <input class="form-control" type="text" id="return" name="return"/>
        </div>
        <br>
        <br/>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection
