@if(session('success'))
    <div class="alert-success">
        {{session('success')}}
    </div>
    @endif

@if(session('error'))
    <div class="alert-danger">
        {{session('error')}}
    </div>
@endif

@if($errors->any())
    <div class="alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
    @endif

