@extends('layouts.base')
@section('content')
    <div class="row">
        <div class="col-6 offset-3 align-self-center">
            @include('notifications.alerts')
            <h1 class="text-center">Get your ticket</h1>
            <hr>
            <form action="{{route('sessions.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="passport_number">Passport Number</label>
                    <input type="passport_number" class="form-control" id="passport_number" aria-describedby="passport_number" name="passport_number">
                    <small id="passport_number" class="form-text text-muted">We'll never share your passport number with anyone else.</small>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                </div>
                <button type="submit" class="btn btn-primary">Get your ticket</button> OR  <a href="{{route('users.register')}}">register for get new ticket</a>
            </form>
        </div>
    </div>
@endsection

