<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[App\Http\Controllers\HomeController::class,'index'])->name('home');
Route::resource('/genres',\App\Http\Controllers\GenresController::class)->only('index','show');

Route::resource('/books',\App\Http\Controllers\BooksController::class)->only('index','show');

Route::get('/users/register',[App\Http\Controllers\UsersController::class,'register'])->name('users.register');
Route::post('/users/register',[App\Http\Controllers\UsersController::class,'store'])->name('users.store');
Route::get('/login',[App\Http\Controllers\SessionController::class,'create'])->name('sessions.login');
Route::post('/login',[App\Http\Controllers\SessionController::class,'store'])->name('sessions.store');
Route::delete('/logout',[App\Http\Controllers\SessionController::class,'destroy'])->name('sessions.delete');
Route::get('/bids',[App\Http\Controllers\BidsController::class, 'create'])->name('bids.create');
Route::post('/bids',[App\Http\Controllers\BidsController::class, 'store'])->name('bids.store');
